const express = require("express")
const app = express()

app.use("/public", express.static("public"))
app.get("/", (req, res) => {
    res.sendFile("sp.html", {root: "."})
})

app.listen(8000, () => {
    console.log(`server running on port 8000`)
})